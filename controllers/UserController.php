<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Users;

class UserController extends ActiveController
{

	public $modelClass = 'app\models\Users';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
			'index' => null, // this overrides the properties set for 'index' in ActiveController
			'create' => null, // this overrides the properties set for 'create' in ActiveController
			//'update' => null, // this overrides the properties set for 'update' in ActiveController
			//'delete' => null, // this overrides the properties set for 'delete' in ActiveController
			'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionView()
	{
		$user = Users::find()->where(['email' => $_GET['email']])->asArray()->one();
		
		echo json_encode($user);
	}
	
	public function actionCreate()
	{
		$response = Users::createUser();
		
		echo json_encode($response);
	}
}
?>
