<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\DeviceStatus;

class DeviceStatusController extends ActiveController
{

	public $modelClass = 'app\models\DeviceStatus';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
				'index' => null, // this overrides the properties set for 'index' in ActiveController
				'create' => null, // this overrides the properties set for 'create' in ActiveController
				//'update' => null, // this overrides the properties set for 'update' in ActiveController
				//'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionCreate()
	{
		$response = DeviceStatus::createDeviceStatusEntry();
		
		echo json_encode($response);
	}
	
	public function actionUpdate()
	{
		$response = DeviceStatus::updateStatus();
	}
	
	public function actionView()
	{
		if(isset($_GET['id']) && $_GET['id'] != '' && isset($_GET['deviceId']) && $_GET['deviceId'] != '' && isset($_GET['switchNumber']) && $_GET['switchNumber'] != '')
		{
			$response = DeviceStatus::getDeviceStatus();
			
			echo json_encode($response);
		}
		else
		{
			echo json_encode(['status' => 'failure']);
		}
	}
	
	public function actionIndex()
	{
		if(isset($_GET['userId']) && $_GET['userId'] != 0)
		{
			$response = DeviceStatus::getSwitchedOnDeviceList();
			
			echo json_encode($response);
		}
		else
		{
			echo json_encode(['status' => 'failure']);
		}
	}
}
?>
