<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\UserRoutines;
use app\models\Utility;

class UserRoutineController extends ActiveController
{

	public $modelClass = 'app\models\UserRoutines';

	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array_merge(parent::actions(),[
			'index' => null, // this overrides the properties set for 'index' in ActiveController
			'create' => null, // this overrides the properties set for 'create' in ActiveController
			'update' => null, // this overrides the properties set for 'update' in ActiveController
			'delete' => null, // this overrides the properties set for 'delete' in ActiveController
			'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}

	public function actionIndex()
	{
		$userRoutines = UserRoutines::getUserRoutines();

		echo json_encode($userRoutines);
	}
	
	public function actionCreate()
	{
		$response = UserRoutines::createUserRoutine();
		
		echo json_encode($response);
	}
	
	public function actionView()
	{
		$userRoutine = UserRoutines::getUserRoutine();
		
		echo json_encode($userRoutine);
	}
	
	public function actionDelete()
	{
		$response = UserRoutines::deleteUserRoutine();
		
		echo json_encode($response);
	}

	public function actionUpdate()
	{
		$response = UserRoutines::updateUserRoutine();

		echo json_encode($response);
	}

	public function actionGetRoutineIdentifier()
	{
		$response = UserRoutines::getRoutineIdentifier();

		echo json_encode($response);
	}
}
?>
