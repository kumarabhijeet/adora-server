<?php

namespace app\controllers;

use yii\rest\Controller;
use app\models\ClubbedDeviceStatus;

class ClubbedDeviceStatusController extends Controller
{
    
    public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		return parent::beforeAction($action);
	}
    
    public function actions()
	{
		return array_merge(parent::actions(),[
				'index' => null, // this overrides the properties set for 'index' in ActiveController
				'create' => null, // this overrides the properties set for 'create' in ActiveController
				//'update' => null, // this overrides the properties set for 'update' in ActiveController
				'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
    
    public function actionView()
    {
        $response = ClubbedDeviceStatus::getClubbedDeviceStatus($_GET['id']);
        echo json_encode($response);
    }
}