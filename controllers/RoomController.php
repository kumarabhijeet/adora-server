<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Rooms;

class RoomController extends ActiveController
{

	public $modelClass = 'app\models\Rooms';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
				'index' => null, // this overrides the properties set for 'index' in ActiveController
				//'create' => null, // this overrides the properties set for 'create' in ActiveController
				//'update' => null, // this overrides the properties set for 'update' in ActiveController
				//'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionIndex()
	{
		$response = Rooms::getAllRooms();
		
		echo json_encode($response);
	}
}
?>
