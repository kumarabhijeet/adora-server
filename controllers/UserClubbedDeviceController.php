<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\UserClubbedDevices;

class UserClubbedDeviceController extends ActiveController
{

	public $modelClass = 'app\models\UserClubbedDevices';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
				'index' => null, // this overrides the properties set for 'index' in ActiveController
				'create' => null, // this overrides the properties set for 'create' in ActiveController
				'update' => null, // this overrides the properties set for 'update' in ActiveController
				'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionIndex()
	{
		if(isset($_GET['userId']) && $_GET['userId'] != '')
		{
			$userClubbedDevices = UserClubbedDevices::getUserClubbedDevices();
			
			echo json_encode($userClubbedDevices);
		}
		else
		{
			echo json_encode([]);
		}
	}
	
	public function actionCreate()
	{
		$response = UserClubbedDevices::createUserClubbedDevice();
		
		echo json_encode($response);
	}
	
	public function actionView()
	{
		$response = UserClubbedDevices::getUserClubbedDevicesItems();
		
		echo json_encode($response);
	}
	
	public function actionDelete()
	{
		$response = UserClubbedDevices::deleteUserClubbedDevice();
		
		echo json_encode($response);
	}
	
	public function actionUpdate()
	{
		parse_str(file_get_contents("php://input"), $putVars);
		
		$response = UserClubbedDevices::updateUserClubbedDevice($putVars);
		
		echo json_encode($response);
	}
}
?>
