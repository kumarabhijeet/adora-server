<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\UserRoomItems;

class UserRoomItemController extends ActiveController
{

	public $modelClass = 'app\models\UserRoomItems';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
				'index' => null, // this overrides the properties set for 'index' in ActiveController
				'create' => null, // this overrides the properties set for 'create' in ActiveController
				'update' => null, // this overrides the properties set for 'update' in ActiveController
				'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionView()
	{
		if($_GET['id'] != 'item' && $_GET['id'] != 'items')
		{
			$response = UserRoomItems::getUserRoomItems(null);	
		}
		elseif($_GET['id'] == 'items')
		{
			$response = UserRoomItems::getUserRoomItems($_GET['ids']);
		}
		else
		{
			$response = UserRoomItems::getUserRoomItem();
		}
		
		echo json_encode($response);
	}
	
	public function actionCreate()
	{
		$response = UserRoomItems::createUserRoomItem();
		
		echo json_encode($response);
	}
	
	public function actionIndex()
	{
		$userRoomsAndItems = UserRoomItems::getAllUserRoomsAndItems();
		
		echo json_encode($userRoomsAndItems);
	}
	
	public function actionUpdate()
	{
		parse_str(file_get_contents("php://input"), $params);
		print_r($params);
	}
	
	public function actionDelete()
	{
		$response = UserRoomItems::deleteUserRoomItem();
		
		echo json_encode($response);
	}

	public function actionGetAllDevices()
	{
		$response = UserRoomItems::getAllDevices();

		echo json_encode($response);
	}
}
?>
