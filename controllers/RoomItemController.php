<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\RoomItems;

class RoomItemController extends ActiveController
{

	public $modelClass = 'app\models\RoomItems';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
				'index' => null, // this overrides the properties set for 'index' in ActiveController
				//'create' => null, // this overrides the properties set for 'create' in ActiveController
				//'update' => null, // this overrides the properties set for 'update' in ActiveController
				//'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionIndex()
	{
		$response = RoomItems::getAllRoomItems();
		
		echo json_encode($response);
	}
}
?>
