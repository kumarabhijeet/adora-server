<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\UserRooms;

class UserRoomController extends ActiveController
{

	public $modelClass = 'app\models\UserRooms';
	
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
		header('Access-Control-Allow-Headers: Content-Type,Accept');
		return parent::beforeAction($action);
	}
	
	public function actions()
	{
		return array_merge(parent::actions(),[
				//'index' => null, // this overrides the properties set for 'index' in ActiveController
				'create' => null, // this overrides the properties set for 'create' in ActiveController
				//'update' => null, // this overrides the properties set for 'update' in ActiveController
				'delete' => null, // this overrides the properties set for 'delete' in ActiveController
				'view' => null, // this overrides the properties set for 'view' in ActiveController
		]);
	}
	
	public function actionView()
	{
		$rooms = UserRooms::getUserRooms();
		
		echo json_encode($rooms);
	}
	
	public function actionCreate()
	{
		$response = UserRooms::createUserRoom();
		
		echo json_encode($response);
	}
	
	public function actionDelete()
	{
		$response = UserRooms::deleteUserRoom();
		
		echo json_encode($response);
	}
}
?>
