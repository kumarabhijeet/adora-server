<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $contact_number
 * @property string $address
 * @property integer $type
 * @property string $unique_id
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'contact_number', 'address', 'type', 'unique_id'], 'required'],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['contact_number'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 500],
            [['unique_id'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'contact_number' => 'Contact Number',
            'address' => 'Address',
            'type' => 'Type',
            'unique_id' => 'Unique ID',
        ];
    }
    
    public function getAllUserInfo()
    {
        
    }
    
    public static function createUser()
    {
        if(isset($_POST['version']) && $_POST['version'] != "")
        {
            $user = new Users();
            $user->unique_id = $_POST['userName'];
            $user->name = $_POST['name'];
            $user->email = $_POST['email'];
            $user->password = md5($_POST['password']);
            $user->save(false);
        }
        else
        {
            $user = new Users();
            $user->name = $_POST['userName'];
            $user->email = $_POST['email'];
            $user->password = md5($_POST['password']);
            $user->save(false);
        }
        
        return ['status' => 'success'];
    }
}