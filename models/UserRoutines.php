<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_routines".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $user_room_item_ids
 * @property string $on_time
 * @property string $off_time
 */
class UserRoutines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_routines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'user_room_item_ids', 'on_time', 'off_time'], 'required'],
            [['user_id'], 'integer'],
            [['on_time', 'off_time'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['user_room_item_ids'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'user_room_item_ids' => 'User Room Item Ids',
            'on_time' => 'On Time',
            'off_time' => 'Off Time',
        ];
    }
    
    public static function createUserRoutine()
    {
        $db = \Yii::$app->db;
        $userId = $_POST['userId'];
        $name = $_POST['name'];
        $userRoomItemIdArray = [];
        
        if(isset($_POST['userRoutineDevices']) && is_array($_POST['userRoutineDevices']))
        {
            foreach($_POST['userRoutineDevices'] as $aDevice)
            {
                if(isset($aDevice['userRoomItemId']) && $aDevice['userRoomItemId'] != '')
                {
                    $userRoomItemIdArray[] = $aDevice['userRoomItemId'];
                }
                else
                {
                    $itemsInRoomSql = 'SELECT * FROM user_room_items WHERE user_room_id = ' . $aDevice['userRoomId'];
                    $itemsInRoomRes = $db->createCommand($itemsInRoomSql)->queryAll();
                    
                    foreach($itemsInRoomRes as $anItem)
                    {
                        $userRoomItemIdArray[] = $anItem['id'];
                    }
                }
            }
            
            if(sizeof($userRoomItemIdArray) > 0)
            {
                $userRoomItemIds = implode(',', $userRoomItemIdArray);
                $onTime = isset($_POST["onTime"]) && $_POST['onTime'] != "" ? date("H:i", strtotime($_POST['onTime'])) : null;
                $offTime = isset($_POST["offTime"]) && $_POST['offTime'] != "" ? date("H:i", strtotime($_POST['offTime'])) : null;
            
                $model = new UserRoutines();
                $model->user_id = $userId;
                $model->name = $name;
                $model->user_room_item_ids = $userRoomItemIds;
                $model->on_time = $onTime;
                $model->off_time = $offTime;
                $model->days_of_the_week = $_POST['days'];
                $model->identifier = $_POST['identifier'];
                $model->save(false);
            }
            
            return ['status' => 'success'];
        }
        else
        {
            return ['status' => 'failure'];
        }
    }
    
    public static function getUserRoutines()
    {
        $db = \Yii::$app->db;
        $userRoutineSql = 'SELECT * FROM user_routines WHERE user_id = ' . $_GET['userId'] . ' ORDER BY id DESC';
        $userRoutineRes = $db->createCommand($userRoutineSql)->queryAll();
        
        if(!is_null($userRoutineRes))
        {
            return ['status' => 'success', 'routines' => $userRoutineRes];   
        }
        else
        {
            return ['status' => 'failure'];
        }
    }
    
    public static function getUserRoutine()
    {
        $db = \Yii::$app->db;
        
        $userRoutineSql = 'SELECT * FROM user_routines WHERE id = ' . $_GET['id'];
        $userRoutineRes = $db->createCommand($userRoutineSql)->queryAll();
        
        if(isset($userRoutineRes[0]))
        {
            if(trim($userRoutineRes[0]['user_room_item_ids']) != '')
            {
                $deviceIdsSql = "SELECT id, name, device_id, switch_number, slave_id FROM user_room_items WHERE id IN ('" . implode("','", explode(',', $userRoutineRes[0]['user_room_item_ids'])) . "')";
                $deviceIdsRes = $db->createCommand($deviceIdsSql)->queryAll();
                
                $userRoutine = ['id' => $userRoutineRes[0]['id'], 'name' => $userRoutineRes[0]['name'], 'deviceIds' => $deviceIdsRes, 'onTime' => $userRoutineRes[0]['on_time'], 'offTime' => $userRoutineRes[0]['off_time'], 'userRoomItemIds' => $userRoutineRes[0]['user_room_item_ids'], 'days' => $userRoutineRes[0]['days_of_the_week']];
                
            }
            else
            {
                $userRoutine = ['id' => $userRoutineRes[0]['id'], 'name' => $userRoutineRes[0]['name'], 'deviceIds' => '', 'onTime' => $userRoutineRes[0]['on_time'], 'offTime' => $userRoutineRes[0]['off_time'], 'userRoomItemIds' => $userRoutineRes[0]['user_room_item_ids'], 'days' => $userRoutineRes[0]['days_of_the_week']];
            }
            
            return ['status' => 'success', 'routine' => $userRoutine];    
        }
        else
        {
            return ['status' => 'failure'];
        }
    }
    
    public static function deleteUserRoutine()
    {
        $result = self::findOne($_GET['id'])->delete();
        
        return ['status' => 'success'];
    }

    public static function updateUserRoutine()
    {
        parse_str(file_get_contents("php://input"), $params);
        
        $routine = self::findOne($_GET['id']);
        $routine->status = $params["status"];
        $routine->save(false);

        return ["status" => "success"];
    }

    public static function getRoutineIdentifier()
    {
        $string = Utility::generateRandomString();

        $db = \Yii::$app->db;
        $uniqueSql = 'SELECT * FROM user_routines WHERE identifier = "' . $string . '"';
        $uniqueRes = $db->createCommand($uniqueSql)->queryAll();

        if(is_array($uniqueRes) && count($uniqueRes) > 0)
        {
            return self::getRoutineIdentifier();
        }
        else
        {
            return ["status" => "success", "identifier" => $string];
        }
    }
}
