<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_coupled_devices".
 *
 * @property integer $id
 * @property string $name
 * @property string $user_room_item_id
 */
class UserCoupledDevices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_coupled_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_room_item_id'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['user_room_item_id'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_room_item_id' => 'User Room Item ID',
        ];
    }
}
