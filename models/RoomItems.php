<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room_items".
 *
 * @property integer $id
 * @property string $name
 */
class RoomItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    
    public static function getAllRoomItems()
    {
        $items = RoomItems::find()->where(1)->asArray()->all();
        
        return ['status' => 'success', 'items' => $items];
    }
}