<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_clubbed_devices".
 *
 * @property integer $id
 * @property string $name
 * @property string $user_room_item_ids
 * @property integer $order
 */
class UserClubbedDevices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_clubbed_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_room_item_ids', 'order'], 'required'],
            [['order'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['user_room_item_ids'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_room_item_ids' => 'User Room Item Ids',
            'order' => 'Order',
        ];
    }
    
    public static function getUserClubbedDevices()
    {
        $db = \Yii::$app->db;
        
        $userClubbedDevicesSql = 'SELECT * FROM user_clubbed_devices WHERE user_id = ' . $_GET['userId'] . ' ORDER BY id DESC';
        $userClubbedDevicesRes = $db->createCommand($userClubbedDevicesSql)->queryAll();
        $userClubbedDevices = [];
        
        foreach($userClubbedDevicesRes as $aClubbedDevice)
        {
            $deviceIdsSql = "SELECT id, name, device_id, switch_number, slave_id FROM user_room_items WHERE id IN ('" . implode("','", explode(',', $aClubbedDevice['user_room_item_ids'])) . "')";
            $deviceIdsRes = $db->createCommand($deviceIdsSql)->queryAll();
            
            if($deviceIdsRes != null)
            {
                $userClubbedDevices[$aClubbedDevice['id']] = ['id' => $aClubbedDevice['id'], 'name' => $aClubbedDevice['name'], 'deviceIds' => $deviceIdsRes, 'userRoomItemIds' => $aClubbedDevice['user_room_item_ids'], "status" => $aClubbedDevice['status'], "actions" => $aClubbedDevice['actions']];
            }
        }
        
        return ['status' => 'success', 'userClubbedDevices' => $userClubbedDevices];
    }
    
    public static function createUserClubbedDevice()
    {
        $db = \Yii::$app->db;
        $userId = $_POST['userId'];
        $clubbedDevicesName = $_POST['name'];
        $userRoomItemIdArray = [];
        if(isset($_POST['version']))
        {
            $actionsArray = [];
        }
        
        if(isset($_POST['userClubbedDevices']) && is_array($_POST['userClubbedDevices']))
        {
            foreach($_POST['userClubbedDevices'] as $aDevice)
            {
                if(isset($aDevice['userRoomItemId']) && $aDevice['userRoomItemId'] != '')
                {
                    $userRoomItemIdArray[] = $aDevice['userRoomItemId'];
                    if(isset($actionsArray))
                    {
                        $actionsArray[] = $aDevice['action'];
                    }
                }
                else
                {
                    $itemsInRoomSql = 'SELECT * FROM user_room_items WHERE user_room_id = ' . $aDevice['userRoomId'];
                    $itemsInRoomRes = $db->createCommand($itemsInRoomSql)->queryAll();
                    
                    foreach($itemsInRoomRes as $anItem)
                    {
                        $userRoomItemIdArray[] = $anItem['id'];
                    }
                }
            }
            
            if(sizeof($userRoomItemIdArray) > 0)
            {
                $userRoomItemIds = implode(',', $userRoomItemIdArray);
            
                $model = new UserClubbedDevices();
                $model->user_id = $userId;
                $model->name = $clubbedDevicesName;
                $model->user_room_item_ids = $userRoomItemIds;
                $model->order = 0;
                $model->actions = isset($actionsArray) ? implode(",", $actionsArray) : "";
                $model->save(false);
            }
        }
        
        return ['status' => 'success'];
    }
    
    public static function getUserClubbedDevicesItems()
    {
        $db = \Yii::$app->db;
        $clubbedDevicesSql = 'SELECT user_room_item_ids, name FROM user_clubbed_devices WHERE user_id = ' . $_GET['userId'] . ' AND id = ' . $_GET['id'];
        $clubbedDevicesRes = $db->createCommand($clubbedDevicesSql)->queryAll();
        
        $clubbedDeviceItemsSql = "SELECT t1.device_id, t1.switch_number, t1.slave_id, t1.name, t2.name AS userRoomName FROM user_room_items AS t1 JOIN user_rooms AS t2 ON t2.id=t1.user_room_id WHERE t1.id IN ('" . implode("','", explode(',', $clubbedDevicesRes[0]['user_room_item_ids'])) . "')";
        $clubbedDeviceItemsRes = $db->createCommand($clubbedDeviceItemsSql)->queryAll();
        
        return ['status' => 'success', 'clubbedDeviceItems' => $clubbedDeviceItemsRes, 'name' => $clubbedDevicesRes[0]['name']];
    }
    
    public static function deleteUserClubbedDevice()
    {
        $model = self::find()->where(['id' => $_GET['id']])->one()->delete();
        
        return ['status' => 'success'];
    }
    
    public static function updateUserClubbedDevice($putVars)
    {
        $db = \Yii::$app->db;
        $id = $_GET['id'];
        if(isset($putVars["version"]) && !isset($putVars["status"]))
        {
            $userRoomItemIdArray = [];
            $actionsArray = [];
            foreach($putVars['userClubbedDevices'] as $aDevice)
            {
                if(isset($aDevice['userRoomItemId']) && $aDevice['userRoomItemId'] != '')
                {
                    $userRoomItemIdArray[] = $aDevice['userRoomItemId'];
                    $actionsArray[] = $aDevice['action'];
                }
                else
                {
                    $itemsInRoomSql = 'SELECT * FROM user_room_items WHERE user_room_id = ' . $aDevice['userRoomId'];
                    $itemsInRoomRes = $db->createCommand($itemsInRoomSql)->queryAll();
                    
                    foreach($itemsInRoomRes as $anItem)
                    {
                        $userRoomItemIdArray[] = $anItem['id'];
                    }
                }
            }

            $name = $putVars["name"];
            $userRoomItemIds = implode(',', $userRoomItemIdArray);
            $actions = implode(',', $actionsArray);

            $userClubbedDeviceSql = "UPDATE user_clubbed_devices SET name = '$name', user_room_item_ids = '$userRoomItemIds', actions = '$actions' WHERE id = $id";
        }
        else if(isset($putVars["version"]) && isset($putVars["status"]))
        {
            $status = $putVars['status'];
            $userClubbedDeviceSql = "UPDATE user_clubbed_devices SET status = $status WHERE id = $id";
        }
        else
        {
            $status = $putVars['status'];
            $userClubbedDeviceSql = "UPDATE user_clubbed_devices SET status = $status WHERE id = $id";
        }

        $userClubbedDeviceRes = $db->createCommand($userClubbedDeviceSql)->execute();
        
        return ["status" => "success"];
    }
}
