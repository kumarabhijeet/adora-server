<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_favorites".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $room_id
 * @property integer $user_room_item_id
 */
class UserFavorites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_favorites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'user_room_id', 'user_room_item_id'], 'required'],
            [['user_id', 'user_room_id', 'user_room_item_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'room_id' => 'Room ID',
            'user_room_item_id' => 'User Room Item ID',
        ];
    }
    
    public static function getUserFavorites()
    {
        $db = \Yii::$app->db;
        $userFavoritesSql = 'SELECT t1.user_room_id as userRoomId, t1.user_room_item_id as userRoomItemId, t2.name as userRoomName, t3.name AS userRoomItemName, t3.device_id AS deviceId, t3.switch_number AS switchNumber, t4.icon AS roomIcon, t5.icon AS roomItemIcon, t5.identifier AS identifier FROM user_favorites AS t1 LEFT JOIN user_rooms as t2 ON t2.id=t1.user_room_id LEFT JOIN user_room_items AS t3 ON t1.user_room_item_id=t3.id LEFT JOIN rooms as t4 ON t4.id=t2.room_id LEFT JOIN room_items AS t5 ON t5.id=t3.room_item_id WHERE t1.user_id = ' . $_GET['userId'] . ' AND t1.user_room_item_id != 0';
        $userFavoritesRes = $db->createCommand($userFavoritesSql)->queryAll();
        
        return ['status' => 'success', 'userFavorites' => $userFavoritesRes];
    }
    
    public static function createUserFavorites()
    {
        $db = \Yii::$app->db;
        $deleteFavoritesSql = "DELETE FROM user_favorites WHERE user_id=" . $_POST['userId'];
        $deleteFavoritesRes = $db->createCommand($deleteFavoritesSql)->execute();
        
        if(isset($_POST['userFavorites']) && is_array($_POST['userFavorites']))
        {
            foreach($_POST['userFavorites'] as $aFavorite)
            {
                $model = new UserFavorites();
                $model->user_id = $_POST['userId'];
                $model->user_room_id = $aFavorite['userRoomId'];
                $model->user_room_item_id = isset($aFavorite['userRoomItemId']) && $aFavorite['userRoomItemId'] != '' ? $aFavorite['userRoomItemId'] : 0;
                $model->order = $aFavorite['order'];
                $model->save(false);
            }    
        }
        
        return ['status' => 'success'];
    }
}
