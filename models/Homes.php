<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "homes".
 *
 * @property integer $id
 * @property string $name
 * @property string $unique_id
 */
class Homes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'homes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'unique_id'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['unique_id'], 'string', 'max' => 25],
            [['unique_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'unique_id' => 'Unique ID',
        ];
    }

    public static function create()
    {
        $string = Utility::generateRandomString();
        $uniqueId = substr($string, 0, 7);

        $model = new Homes();
        $model->name = "My Home";
        $model->unique_id = $uniqueId;
        $model->save();

        return ["code" => $uniqueId];
    }

    public static function getHome()
    {
        $model = Homes::find()->where([ "unique_id" => $_GET['id'] ])->one();

        if(!is_null($model))
        {
            return ["status" => "success", "home" => $model->attributes];
        }
        else
        {
            return ["status" => "failure"];
        }
    }
}
