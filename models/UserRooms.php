<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_rooms".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $room_id
 * @property string $name
 */
class UserRooms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'room_id', 'name'], 'required'],
            [['user_id', 'room_id'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'room_id' => 'Room ID',
            'name' => 'Name',
        ];
    }
    
    public static function getUserRooms()
    {
        $db = \Yii::$app->db;
        $userRoomsSql = 'SELECT t1.id as id, t1.name as userRoomName, room_id as roomId, t2.name as roomName, icon as roomIcon, identifier FROM user_rooms as t1 JOIN rooms as t2 ON t2.id=t1.room_id WHERE t1.user_id = ' . $_GET['id'] . ' ORDER BY t1.id DESC';
        $userRoomsRes = $db->createCommand($userRoomsSql)->queryAll();
        
        return ['status' => 'success', 'userRooms' => $userRoomsRes];
    }
    
    public static function createUserRoom()
    {
        $db = \Yii::$app->db;
        $userRoomsSql = 'SELECT COUNT(*) as count FROM user_rooms WHERE user_id = "' . $_POST['userId'] . '" AND room_id = "' . $_POST['roomId'] . '"';
        $userRoomsRes = $db->createCommand($userRoomsSql)->queryAll();
        
        $roomName = $_POST['roomName'] . ($userRoomsRes[0]['count'] + 1);
        $userId = $_POST['userId'];
        $userRoom = new UserRooms();
        $userRoom->user_id = $userId;
        $userRoom->room_id = $_POST['roomId'];
        $userRoom->name = $roomName;
        $userRoom->save(false);
        
        $room = Rooms::find()->where(['id' => $_POST['roomId']])->asArray()->one();
        
        return ['status' => 'success', 'id' => $userRoom->id, 'name' => $userRoom->name, 'room' => $room];
    }
    
    public static function deleteUserRoom()
    {
        $result = self::findOne($_GET['id'])->delete();
        
        return ["status" => "success"];
    }
}
