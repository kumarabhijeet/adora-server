<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_status".
 *
 * @property integer $id
 * @property string $device_id
 * @property integer $status
 */
class DeviceStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_id', 'status'], 'required'],
            [['status'], 'integer'],
            [['device_id'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'status' => 'Status',
        ];
    }
    
    public static function createDeviceStatusEntry()
    {
        $model = self::find()->where(['user_id' => $_POST['userId'], 'device_id' => $_POST['deviceId'], 'switch_number' => $_POST['switchNumber']])->one();
        if(!is_object($model))
        {
            $model = new DeviceStatus();
            $model->user_id = $_POST['userId'];
            $model->device_id = $_POST['deviceId'];
            $model->switch_number = $_POST['switchNumber'];
            $model->user_room_item_id = $_POST['userRoomItemId'];
        }
        $model->status = $_POST['status'];
        
        $model->save(false);
        
        if(isset($model->id))
        {
            return ['status' => 'success'];
        }
        else
        {
            return ['status' => 'failure'];
        }
    }
    
    public static function getDeviceStatus()
    {
        $db = \Yii::$app->db;
        
        $deviceStatusSql = 'SELECT * FROM device_status WHERE user_id = ' . $_GET['id'] . ' AND device_id = ' . $_GET['deviceId'] . ' AND switch_number = ' . $_GET['switchNumber'] . ' DESC LIMIT 1';
        $deviceStatusRes = $db->createCommand($deviceStatusSql)->query();
        
        return ['status' => 'success', 'deviceStatus' => $deviceStatusRes->device_id];
    }
    
    public static function getSwitchedOnDeviceList()
    {
        $db = \Yii::$app->db;
        
        $switchedOnDevicesSql = 'SELECT t1.user_id, t1.name AS roomName, t2.id AS userRoomItemId, t2.name, t2.device_id, t2.switch_number, t3.status FROM user_rooms AS t1 LEFT JOIN user_room_items AS t2 ON t1.id = t2.user_room_id LEFT JOIN device_status AS t3 ON t2.id = t3.user_room_item_id WHERE t3.user_id = ' . $_GET['userId'];
        $switchedOnDevicesRes = $db->createCommand($switchedOnDevicesSql)->queryAll();
        
        return ['status' => 'success', 'switchedOnDevices' => $switchedOnDevicesRes];
    }
}
