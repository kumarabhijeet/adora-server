<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_room_items".
 *
 * @property integer $id
 * @property integer $user_room_id
 * @property string $name
 */
class UserRoomItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_room_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_room_id', 'name'], 'required'],
            [['user_room_id'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_room_id' => 'User Room ID',
            'name' => 'Name',
        ];
    }
    
    public static function createUserRoomItem()
    {
        if(isset($_POST['version']))
        {
            $model = new UserRoomItems();
            $model->user_room_id = $_POST['userRoomId'];
            $model->room_item_id = $_POST['roomItemId'];
            $model->device_id = $_POST['deviceId'];
            $model->slave_id = "";
            $model->switch_number = $_POST['switchNumber'];
            $model->name = $_POST['name'];
            $model->save(false);

            return ['status' => 'success', 'id' => $model->id];    
        }
        else if(isset($_POST['deviceId']))
        {
            $response = [];
            $exisingRoomItem = UserRoomItems::find()->where(['device_id' => $_POST['deviceId'], 'switch_number' => $_POST['switchNumber']])->one();
            if(!is_object($exisingRoomItem))
            {
                $userRoomItem = UserRoomItems::find()->where(['id' => $_POST['roomItemId']])->all();
                $userRoomItem[0]->device_id = $_POST['deviceId'];
                $userRoomItem[0]->switch_number = $_POST['switchNumber'];
                $userRoomItem[0]->save(false);
                
                $response = ['status' => 'success'];
            }
            else
            {
                $response = ['status' => 'failure', 'reason' => 'duplicate'];
            }
            
            return $response;
        }
        elseif(isset($_POST['slaveId']))
        {
            $userRoomItem = UserRoomItems::find()->where(['id' => $_POST['roomItemId']])->all();
            $userRoomItem[0]->slave_id = $_POST['slaveId'];
            $userRoomItem[0]->save(false);
            
            return ['status' => 'success']; 
        }
        else
        {
            $db = \Yii::$app->db;
            $userRoomItemSql = 'SELECT COUNT(*) as count FROM user_room_items WHERE user_room_id = "' . $_POST['userRoomId'] . '" AND room_item_id = "' . $_POST['roomItemId'] . '"';
            $userRoomItemRes = $db->createCommand($userRoomItemSql)->queryAll();
            
            $model = new UserRoomItems();
            $model->user_room_id = $_POST['userRoomId'];
            $model->room_item_id = $_POST['roomItemId'];
            $model->device_id = '';
            $model->name = isset($_POST['roomItemName']) ? $_POST['roomItemName'] . ($userRoomItemRes[0]['count'] + 1) : '';
            $model->save(false);
            
            $roomItem = RoomItems::find()->where(['id' => $_POST['roomItemId']])->asArray()->one();
            
            return ['status' => 'success', 'id' => $model->id, 'name' => $model->name, 'item' => $roomItem];    
        }
    }
    
    public static function getUserRoomItems($ids)
    {
        $db = \Yii::$app->db;
        
        if($ids == null)
        {
            $roomItemsSql = 'SELECT t1.id AS id, t1.user_room_id AS user_room_id, t1.room_item_id AS room_item_id, t1.device_id AS device_id, t1.switch_number AS switch_number, t1.name AS name, t1.slave_id AS slave_id, t2.icon AS icon, t2.identifier AS identifier FROM user_room_items AS t1 JOIN room_items AS t2 ON t1.room_item_id=t2.id WHERE t1.user_room_id=' . $_GET['id'];
        }
        else 
        {
            $roomItemsSql = 'SELECT t1.id AS userRoomItemId, t1.user_room_id AS userRoomId, t1.room_item_id AS roomItemId, t1.device_id AS deviceId, t1.switch_number AS switchNumber, t1.name AS userRoomItemName, t1.slave_id AS slaveId, t2.icon AS userRoomItemIcon, t2.identifier AS identifier, t3.name AS userRoomName, t3.id AS userRoomId FROM user_room_items AS t1 JOIN room_items AS t2 ON t1.room_item_id=t2.id JOIN user_rooms AS t3 ON t1.user_room_id=t3.id WHERE t1.id IN (' . $ids . ')';
        }
        
        $roomItemsRes = $db->createCommand($roomItemsSql)->queryAll();
            
        if(!is_null($roomItemsRes))
        {
            return ['status' => 'success', 'roomItems' => $roomItemsRes];    
        }
        else
        {
            return ['status' => 'failure'];    
        }
    }
    
    public static function getUserRoomItem()
    {
        $userRoomItem = UserRoomItems::find()->where(['id' => $_GET['userRoomItemId']])->asArray()->all();
        
        if(!is_null($userRoomItem) || !empty($userRoomItem))
        {
            return ['status' => 'success', 'userRoomItem' => $userRoomItem[0]];    
        }
        else
        {
            return ['status' => 'failure'];    
        }
    }
    
    public static function getAllUserRoomsAndItems()
    {
        $db = \Yii::$app->db;
        
        if(isset($_GET['itemInfo']) && $_GET['itemInfo'] == true)
        {
            $userRoomsAndItemSql = 'SELECT t1.room_id as roomId, t1.id as userRoomId, t1.name as userRoomName, t2.id as userRoomItemId, t2.device_id as deviceId, t2.switch_number as switchNumber, t2.name as userRoomItemName, t3.icon as roomIcon, t4.icon as roomItemIcon FROM user_rooms AS t1 LEFT JOIN user_room_items AS t2 ON t1.id=t2.user_room_id JOIN rooms AS t3 ON t1.room_id=t3.id LEFT JOIN room_items AS t4 ON t2.room_item_id=t4.id WHERE t1.user_id = ' . $_GET['userId'];
        }
        else
        {
            $userRoomsAndItemSql = 'SELECT t1.room_id as roomId, t1.id as userRoomId, t1.name as userRoomName, t2.id as userRoomItemId, t2.name as userRoomItemName, t3.icon as roomIcon, t4.icon as roomItemIcon FROM user_rooms AS t1 LEFT JOIN user_room_items AS t2 ON t1.id=t2.user_room_id JOIN rooms AS t3 ON t1.room_id=t3.id LEFT JOIN room_items AS t4 ON t2.room_item_id=t4.id WHERE t1.user_id = ' . $_GET['userId'];   
        }
        $userRoomsAndItemRes = $db->createCommand($userRoomsAndItemSql)->queryAll();    
        $roomsAndItems = [];
        
        foreach($userRoomsAndItemRes as $aRow)
        {
            if(!array_key_exists($aRow['userRoomId'], $roomsAndItems))
            {
               $roomsAndItems[$aRow['userRoomId']] = []; 
            }
            if($aRow['userRoomItemId'] !== NULL)
            {
                $roomsAndItems[$aRow['userRoomId']][] = $aRow;
            }
            else if ($aRow['userRoomItemId'] === NULL)
            {
                
            }
            else
            {
                $roomsAndItems[$aRow['userRoomId']] = $aRow;
            }
        }
        
        return ['status' => 'success', 'roomsAndItems' => $roomsAndItems];
    }
    
    public static function deleteUserRoomItem()
    {
        $userRoomItemId = $_GET['id'];
        $db = \Yii::$app->db;
        $userRoutineSql = "SELECT id, user_room_item_ids FROM user_routines WHERE FIND_IN_SET(" . $userRoomItemId . ", user_room_item_ids)";
        $userRoutineRes = $db->createCommand($userRoutineSql)->queryAll();
        
        foreach($userRoutineRes as $aRoutine)
        {
            $ids = explode(",", $aRoutine['user_room_item_ids']);
            $key = array_search($userRoomItemId, $ids);
            if($key != false)
            {
                unset($ids[$key]);
            }
            $userRoutineUpdateSql = "UPDATE user_routines SET user_room_item_ids = '" . implode(",", $ids) . "' WHERE id = " . $aRoutine['id'];
            $userRoutineUpdateRes = $db->createCommand($userRoutineUpdateSql)->execute();
        }
        
        $userClubbedDevicesSql = "SELECT id, user_room_item_ids FROM user_clubbed_devices WHERE FIND_IN_SET(" . $userRoomItemId . ", user_room_item_ids)";
        $userClubbedDevicesRes = $db->createCommand($userClubbedDevicesSql)->queryAll();
        
        foreach($userClubbedDevicesRes as $userClubbedDevice)
        {
            $ids = explode(",", $userClubbedDevice['user_room_item_ids']);
            $key = array_search($userRoomItemId, $ids);
            if($key != false)
            {
                unset($ids[$key]);
            }
            $userClubbedDeviceUpdateSql = "UPDATE user_clubbed_devices SET user_room_item_ids = '" . implode(",", $ids) . "' WHERE id = " . $aRoutine['id'];
            $userClubbedDeviceUpdateRes = $db->createCommand($userClubbedDeviceUpdateSql)->execute();
        }
        
        $switchedDeviceSql = "DELETE FROM device_status WHERE user_room_item_id = " . $userRoomItemId;
        $switchedDeviceRes = $db->createCommand($switchedDeviceSql)->execute();
        
        $userFavoritesSql = "DELETE FROM user_favorites WHERE user_room_id = " . $userRoomItemId;
        $userFavoritesRes = $db->createCommand($userFavoritesSql)->execute();
        
        $result = self::find()->where(['id' => $_GET['id']])->one()->delete();
        
        return ['status' => 'success'];
    }

    public static function getAllDevices()
    {
        $db = \Yii::$app->db;

        $devicesSql = 'SELECT DISTINCT device_id FROM user_room_items WHERE user_room_id IN ( SELECT id FROM user_rooms WHERE user_id = ' . $_GET['id'] . ')';
        $devicesRes = $db->createCommand($devicesSql)->queryAll();
        $devices = [];

        foreach($devicesRes as $aDevice)
        {
            $devices[] = $aDevice['device_id'];
        }

        return ['status' => 'success', 'devices' => $devices];
    }
}